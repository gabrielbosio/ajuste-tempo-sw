# Ajuste del tempo de una pista de audio en base al tempo de una señal de audio externa
## Prototipo de Software

Antes de usar, ejecutar las siguientes instrucciones bash:  
`sudo apt-get update -qy`  
`sudo apt-get install -qy python3-dev python3-pip portaudio19-dev`  
`pip3 install -r requirements.txt`  

Es necesario tener un micrófono como entrada de audio.  
Se recomienda usar auriculares como salida de audio para que la señal de salida no interfiera con la señal de entrada.  
  
Ejecutar usando:  
`python3 .`
