import wave
from src.loop_audio import LoopAudio
from src.interfaz_audio import InterfazAudio
from src.ajustador_tempo import AjustadorTempo


def entrada_salida(buffer_a_procesar, buffer_procesado, nombre_archivo):
    archivo_loop = wave.open(nombre_archivo, 'rb')
    loop_audio = LoopAudio(archivo_loop)
    interfaz_audio = InterfazAudio(loop_audio, buffer_a_procesar, buffer_procesado)
    interfaz_audio.iniciar()
    interfaz_audio.finalizar()
    archivo_loop.close()


def ajuste_tempo(buffer_a_procesar, buffer_procesado):
    ajustador = AjustadorTempo()
    ajustador.ejecutar(buffer_a_procesar, buffer_procesado)
