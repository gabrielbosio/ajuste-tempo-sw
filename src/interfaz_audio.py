import pyaudio
import time
from src.lector import LectorAudio


class InterfazAudio():
    def __init__(self, loop_audio, buffer_a_procesar, buffer_procesado):
        self.buffer_a_procesar = buffer_a_procesar
        self.buffer_procesado = buffer_procesado
        self.ratio = 1
        self.grabacion = bytes()
        self.lector = LectorAudio(loop_audio)
        self.frecuencia_muestreo = loop_audio.frecuencia_muestreo
        self.motor_audio = pyaudio.PyAudio()
        self.flujo_audio = self.motor_audio.open(format=pyaudio.paInt16,
                                                 channels=loop_audio.canales,
                                                 rate=self.frecuencia_muestreo,
                                                 input=True,
                                                 output=True,
                                                 stream_callback=self.__callback)
    
    def iniciar(self):
        self.flujo_audio.start_stream()

        while self.flujo_audio.is_active():
            time.sleep(0.1)

    def finalizar(self):
        self.flujo_audio.stop_stream()
        self.flujo_audio.close()
        self.motor_audio.terminate()

    def __callback(self, entrada, cantidad_frames, time_info, status):
        salida = self.lector.leer(cantidad_frames)
        self.grabacion += entrada

        if self.lector.leyo_pagina:
            if not self.buffer_a_procesar.full():
                self.buffer_a_procesar.put((self.lector.proxima_pagina(), self.grabacion,
                                            self.frecuencia_muestreo, self.ratio))
        
            self.grabacion = bytes()
        
            if not self.buffer_procesado.empty():
                audio_escalado, ratio = self.buffer_procesado.get()
                self.ratio = ratio
                self.lector.agregar_audio_escalado(audio_escalado)

        return salida, pyaudio.paContinue
