import numpy as np


class LectorAudio():

    def __init__(self, loop_audio, cantidad_frames_fade=100):
        self.loop_audio = loop_audio
        self.audio_escalado = bytes()
        self.numero_pagina_actual = 0
        self.puntero_buffer = 0
        self.leyo_pagina = False
        self.cantidad_frames_fade = cantidad_frames_fade
        self.fade_out = np.zeros(cantidad_frames_fade)
    
    def pagina_actual(self):
        u"""Si audio_escalado está vacío, devuelve la página actual de loop_audio. De lo
        contrario, devuelve audio_escalado.
        """
        if len(self.audio_escalado) > 0:
            pagina_actual = self.audio_escalado
        else:
            pagina_actual = self.loop_audio.pagina(self.numero_pagina_actual).tobytes()
        
        return pagina_actual
    
    def proxima_pagina(self):
        u"""Devuelve la próxima página de loop_audio. Reestablece leyo_pagina."""
        proxima_pagina = self.loop_audio.pagina(self.numero_pagina_actual + 1).tobytes()        
        self.leyo_pagina = False
        
        return proxima_pagina
    
    def agregar_audio_escalado(self, audio_escalado):
        self.audio_escalado += audio_escalado
    
    def leer(self, cantidad_frames):
        u"""Devuelve una parte de la página que está leyendo actualmente, dada por
        pagina_actual.
        cantidad_frames -- La cantidad de frames de la parte de la página a devolver."""
        cantidad_muestras = cantidad_frames * 4
        cantidad_muestras_fade = self.cantidad_frames_fade * 8
        fin_buffer = self.puntero_buffer + cantidad_muestras + cantidad_muestras_fade
        pagina_actual = self.pagina_actual()
        buffer_actual = pagina_actual[self.puntero_buffer : fin_buffer]
        offset_con_fade = fin_buffer - len(pagina_actual)
        offset_sin_fade = offset_con_fade - cantidad_muestras_fade

        if offset_sin_fade > 0:
            self.leyo_pagina = True
            self.audio_escalado = bytes()
            self.numero_pagina_actual += 1
            buffer_actual += self.pagina_actual()[:offset_con_fade]
            self.puntero_buffer = offset_sin_fade
        else:
            if offset_con_fade > 0:
                buffer_actual += self.proxima_pagina()[:offset_con_fade]
            self.puntero_buffer += cantidad_muestras
                
        return buffer_actual
    
    def __fundir_pagina(self, buffer):
        vector_buffer = np.frombuffer(buffer, dtype=np.float64)
        rampa = np.linspace(0, 1, self.cantidad_frames_fade)
        fade_in = vector_buffer[:self.cantidad_frames_fade] * rampa + self.fade_out
        fade_out = vector_buffer[-self.cantidad_frames_fade:] * rampa[::-1]
        resto = vector_buffer[self.cantidad_frames_fade : -self.cantidad_frames_fade]
        self.fade_out = fade_out
        vector_fundido = np.hstack((fade_in, resto))
        
        return vector_fundido.tobytes()
