import numpy as np
from src.compuerta import Compuerta
from src.detector import DetectorTempo
from src.comparador import ComparadorTempos
from src.escalador import EscaladorTiempo


class AjustadorTempo:
    def __init__(self):
        self.compuerta = Compuerta(umbral = 0.1 * 2**15)
        self.detector = DetectorTempo()
        self.comparador = ComparadorTempos()
        self.escalador = EscaladorTiempo()
        self.tempo_grabacion = 120
        self.tempo_lectura = -1
        self.tempo_anterior = self.tempo_grabacion

    def ejecutar(self, buffer_a_procesar, buffer_procesado):
        while True:
            if not buffer_a_procesar.empty():
                lectura_loop, grabacion, frec_muestreo, ratio = buffer_a_procesar.get()
                grabacion = np.frombuffer(grabacion, dtype=np.int16)
                grabacion = self.compuerta.filtrar(grabacion)
                tempo_actual = self.detector.ejecutar(grabacion, frec_muestreo)
                tempos_coinciden = self.comparador.tempos_coinciden(tempo_actual,
                                                                    self.tempo_anterior)
                mensaje = str(tempo_actual)

                if tempo_actual <= 0 or tempos_coinciden:
                    tempo_actual = self.tempo_anterior
                else:
                    tempo_actual *= self.comparador.multiplo(tempo_actual,
                                                             self.tempo_anterior)
                    mensaje += f" -> {tempo_actual}"
                
                print(mensaje)
                ratio = tempo_actual / self.tempo_grabacion
                vector_loop = np.frombuffer(lectura_loop, dtype=np.int16)
                vector_loop = vector_loop.astype(np.float64)
                vector_loop = self.escalador.ejecutar(vector_loop, ratio)
                lectura_loop = vector_loop.astype(np.int16).tobytes()
                self.tempo_anterior = tempo_actual
                
                if not buffer_procesado.full():
                    buffer_procesado.put((lectura_loop, ratio))
