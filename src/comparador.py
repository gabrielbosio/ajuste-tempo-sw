import math


class ComparadorTempos:

    def __init__(self, umbral_minimo=5, umbral_polirritmico=[1, 2, 4]):
        if umbral_minimo < 0:
            raise UmbralMinimoException()
        
        for umbral in umbral_polirritmico:
            if umbral < 0:
                raise UmbralPolirritmicoException()

        self.umbral_minimo = umbral_minimo
        self.umbral_polirritmico = umbral_polirritmico
    
    def tempos_coinciden(self, tempo1, tempo2):
        u"""Devuelve True si los múltiplos de los tempos de umbral_polirritmico difieren
        en un rango menor a umbral_minimo. De lo contrario, devuelve False.
        """
        porcentaje_umbral = self.umbral_minimo / 100

        for umbral in self.umbral_polirritmico:
            if (math.fabs(tempo1 - tempo2 * umbral) <= tempo1 * porcentaje_umbral or
                math.fabs(tempo2 - tempo1 * umbral) <= tempo2 * porcentaje_umbral):
                return True
        
        return False
    
    def multiplo(self, tempo1, tempo2):
        u"""Devuelve el múltiplo de umbral_polirritmico que implique la menor diferencia
        entre los tempos."""
        diferencias = {}

        for umbral in self.umbral_polirritmico:
            diferencias[1 / umbral] = math.fabs(tempo1 - tempo2 * umbral)
            diferencias[umbral] = math.fabs(tempo2 - tempo1 * umbral)
        
        return min(diferencias, key=diferencias.get)


class UmbralMinimoException(Exception):
    
    def __init__(self):
        super().__init__("El umbral mínimo debe ser positivo o 0.")


class UmbralPolirritmicoException(Exception):
    
    def __init__(self):
        super().__init__("Todos los valores del umbral polirrítmico deben ser " +
                         "positivos o 0.")
