import numpy as np
import wave
import pytest
from detector import DetectorTempo
from pytest import approx


@pytest.fixture
def segundos_ventana():
    return 3


@pytest.fixture
def delta():
    return 1


@pytest.fixture
def hihat_120(segundos_ventana):
    return hihat("res/test_hihat_120.wav", segundos_ventana)


@pytest.fixture
def hihat_55(segundos_ventana):
    return hihat("res/test_hihat_55.wav", segundos_ventana)


@pytest.fixture
def hihat_220(segundos_ventana):
    return hihat("res/test_hihat_220.wav", segundos_ventana)


def test_detectar_tempo_ventana_hihat_a_120_bpm(hihat_120, delta):
    ventana_hihat, frecuencia_muestreo = hihat_120
    detector_tempo = DetectorTempo()
    tempo = detector_tempo.ejecutar(ventana_hihat, frecuencia_muestreo)

    assert tempo == approx(120, abs=delta)


def test_ventana_hihat_a_55_bpm_detecta_110bpm(hihat_55, delta):
    ventana_hihat, frecuencia_muestreo = hihat_55
    detector_tempo = DetectorTempo()
    tempo = detector_tempo.ejecutar(ventana_hihat, frecuencia_muestreo)

    assert tempo == approx(110, abs=delta)


def test_detectar_tempo_ventana_hihat_a_220_bpm(hihat_220, delta):
    ventana_hihat, frecuencia_muestreo = hihat_220
    detector_tempo = DetectorTempo()
    tempo = detector_tempo.ejecutar(ventana_hihat, frecuencia_muestreo)

    assert tempo == approx(220, abs=delta)


def test_detectar_tempo_ventana_silencio(segundos_ventana):
    fs_silencio = 44100
    muestras_ventana = fs_silencio * segundos_ventana
    ventana_silencio = np.zeros(muestras_ventana)
    detector_tempo = DetectorTempo()
    tempo = detector_tempo.ejecutar(ventana_silencio, fs_silencio)

    assert tempo < 0


def hihat(ruta, segundos_ventana):
    archivo_hihat = wave.open(ruta)
    frecuencia_muestreo = archivo_hihat.getframerate()
    muestras_ventana = frecuencia_muestreo * segundos_ventana
    audio_hihat = archivo_hihat.readframes(archivo_hihat.getnframes())
    audio_hihat = np.frombuffer(audio_hihat, dtype=np.int16)
    ventana_hihat = audio_hihat[:muestras_ventana]

    return ventana_hihat, frecuencia_muestreo
