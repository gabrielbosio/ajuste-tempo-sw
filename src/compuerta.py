import numpy as np

class Compuerta:
    
    def __init__(self, umbral=0.02):
        self.umbral = umbral
    
    def filtrar(self, x):
        return np.where(np.abs(x) < self.umbral, 0, x)
