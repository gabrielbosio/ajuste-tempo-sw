import pytest
from comparador import *


def test_comparar_tempos_con_umbral_minimo_de_5_porciento():
    comparador = ComparadorTempos(umbral_minimo=5)

    assert not comparador.tempos_coinciden(120, 130)
    assert comparador.tempos_coinciden(120, 126)
    assert comparador.tempos_coinciden(120, 124)


def test_comparar_tempos_sin_umbral_minimo():
    comparador = ComparadorTempos(umbral_minimo=0)

    assert comparador.tempos_coinciden(120, 120)
    assert not comparador.tempos_coinciden(120, 120.01)


def test_comparar_tempos_con_umbral_minimo_negativo():
    with pytest.raises(UmbralMinimoException):
        comparador = ComparadorTempos(umbral_minimo=-10)


def test_comparar_tempos_con_umbral_polirritmico_de_multiplos_de_2():
    comparador = ComparadorTempos(umbral_minimo=0,
                                  umbral_polirritmico=[1, 2, 4])

    assert comparador.tempos_coinciden(120, 240)
    assert comparador.tempos_coinciden(60, 240)
    assert not comparador.tempos_coinciden(60, 241)


def test_comparar_tempos_con_umbral_polirritmico_de_2_y_umbral_minimo_de_5():
    comparador = ComparadorTempos(umbral_minimo=5,
                                  umbral_polirritmico=[1, 2, 4])

    assert comparador.tempos_coinciden(120, 246)
    assert not comparador.tempos_coinciden(120, 253)
    assert comparador.tempos_coinciden(60, 126)
    assert not comparador.tempos_coinciden(60, 127)
    assert comparador.tempos_coinciden(120, 57)
    assert not comparador.tempos_coinciden(120, 56)


def test_comparar_tempos_con_un_valor_de_umbral_polirritmico_negativo():
    with pytest.raises(UmbralPolirritmicoException):
        comparador = ComparadorTempos(umbral_polirritmico=[1, 2, -4])


def test_obtener_multiplo():
    comparador = ComparadorTempos(umbral_polirritmico=[1, 2, 4])
    
    assert comparador.multiplo(120, 125) == 1
    assert comparador.multiplo(120, 63) == 0.5
    assert comparador.multiplo(120, 245) == 2
    assert comparador.multiplo(120, 180) == 1
