import numpy as np
from numpy.lib.stride_tricks import as_strided
from scipy import signal

# Copyright (c) 2013--2017, librosa development team.

MAX_MEM_BLOCK = 2**8 * 2**10

class EscaladorTiempo:

    def ejecutar(self, x, ratio):
        u"""Devuelve el vector de audio x escalado en tiempo dada la proporción ratio."""
        stft = self.__stft(x)
        stft_stretch = self.__phase_vocoder(stft, ratio)
        
        len_stretch = int(round(len(x) / ratio))
        x_stretch = self.__istft(stft_stretch, dtype=x.dtype, length=len_stretch)

        return x_stretch
    
    def __stft(self, y):
        win_length = 2048
        fft_window = signal.get_window('hann', win_length, fftbins=True)
        fft_window = self.__pad_center(fft_window, win_length)
        fft_window = fft_window.reshape((-1, 1))

        if win_length > y.shape[-1]:
            print('win_length={} is too small for input signal of length={}'.format(win_length, y.shape[-1]))

        # Aplicar padding asi las ventanas quedan centradas
        y = np.pad(y, int(win_length // 2), mode='reflect')

        # Dividir la entrada en ventanas
        y_frames = self.__frame(y, frame_length=win_length, hop_length=win_length // 4)

        stft_matrix = np.empty((1 + win_length // 2, y_frames.shape[1]), dtype=np.complex128,
                               order='F')

        # Cantidad maxima de columnas
        n_columns = MAX_MEM_BLOCK // (stft_matrix.shape[0] * stft_matrix.itemsize)
        n_columns = max(n_columns, 1)

        for bl_s in range(0, stft_matrix.shape[1], n_columns):
            bl_t = min(bl_s + n_columns, stft_matrix.shape[1])

            stft_matrix[:, bl_s:bl_t] = np.fft.rfft(fft_window * y_frames[:, bl_s:bl_t],
                                                    axis=0)
        return stft_matrix
    
    def __phase_vocoder(self, D, rate):
        n_fft = 2 * (D.shape[0] - 1)
        hop_length = n_fft // 4
        time_steps = np.arange(0, D.shape[1], rate, dtype=np.float)

        d_stretch = np.zeros((D.shape[0], len(time_steps)), D.dtype, order='F')

        # Avance de fase esperado
        phi_advance = np.linspace(0, np.pi * hop_length, D.shape[0])

        # Acumulador de fase inicial
        phase_acc = np.angle(D[:, 0])

        # Simplificar logica de bordes
        D = np.pad(D, [(0, 0), (0, 2)], mode='constant')

        for (t, step) in enumerate(time_steps):
            columns = D[:, int(step):int(step + 2)]

            # Magnitud de interpolacion lineal
            alpha = np.mod(step, 1.0)
            mag = ((1.0 - alpha) * np.abs(columns[:, 0]) + alpha * np.abs(columns[:, 1]))

            # Almacenar en array de salida
            d_stretch[:, t] = mag * np.exp(1.j * phase_acc)

            # Calcular avance de fase, ajustando aun rango [-pi, pi]
            dphase = (np.angle(columns[:, 1]) - np.angle(columns[:, 0]) - phi_advance)
            dphase -= 2.0 * np.pi * np.round(dphase / (2.0 * np.pi))
            phase_acc += phi_advance + dphase

        return d_stretch
    
    def __istft(self, stft_matrix, dtype=np.float64, length=None):
        win_length = 2 * (stft_matrix.shape[0] - 1)
        hop_length = int(win_length // 4)
        ifft_window = signal.get_window('hann', win_length, fftbins=True)
        ifft_window = self.__pad_center(ifft_window, win_length)[:, np.newaxis]

        # Igualar longitud de ventanas de STFT para mas eficiencia
        if length:
            padded_length = length + int(win_length)
            n_frames = min(stft_matrix.shape[1], int(np.ceil(padded_length / hop_length)))
        else:
            n_frames = stft_matrix.shape[1]

        expected_signal_len = win_length + hop_length * (n_frames - 1)

        y = np.zeros(expected_signal_len, dtype=dtype)

        n_columns = MAX_MEM_BLOCK // (stft_matrix.shape[0] * stft_matrix.itemsize)
        n_columns = max(n_columns, 1)

        frame = 0
        for bl_s in range(0, n_frames, n_columns):
            bl_t = min(bl_s + n_columns, n_frames)

            # Aplicar la Transformada inversa con la funcion ventana
            ytmp = ifft_window * np.fft.irfft(stft_matrix[:, bl_s:bl_t], axis=0)
            self.__overlap_add(y[frame * hop_length:], ytmp, hop_length)

            frame += bl_t - bl_s

        # Normalizar por la suma de los cuadrados de la ventana
        ifft_window_sum = self.__window_sumsquare('hann',
                                                n_frames,
                                                win_length=win_length,
                                                n_fft=win_length,
                                                hop_length=hop_length,
                                                dtype=dtype)

        approx_nonzero_indices = ifft_window_sum > np.finfo(ifft_window_sum.dtype).tiny
        y[approx_nonzero_indices] /= ifft_window_sum[approx_nonzero_indices]
        y = y[int(win_length // 2):-int(win_length // 2)]
        
        return y
    
    def __pad_center(self, data, size):
        axis = -1
        n = data.shape[axis]
        lpad = int((size - n) // 2)

        lengths = [(0, 0)] * data.ndim
        lengths[axis] = (lpad, int(size - n - lpad))

        if lpad < 0:
            raise ParameterError(('Target size ({:d}) must be '
                                'at least input size ({:d})').format(size, n))

        return np.pad(data, lengths, mode='constant')
    
    def __frame(self, x, frame_length, hop_length):
        axis = -1

        if x.shape[axis] < frame_length:
            raise ParameterError('Input is too short (n={:d})'
                                ' for frame_length={:d}'.format(x.shape[axis], frame_length))

        if hop_length < 1:
            raise ParameterError('Invalid hop_length: {:d}'.format(hop_length))

        n_frames = 1 + (x.shape[axis] - frame_length) // hop_length
        strides = np.asarray(x.strides)

        new_stride = np.prod(strides[strides > 0] // x.itemsize) * x.itemsize

        shape = list(x.shape)[:-1] + [frame_length, n_frames]
        strides = list(strides) + [hop_length * new_stride]

        return as_strided(x, shape=shape, strides=strides)
    
    def __overlap_add(self, y, ytmp, hop_length):
        n_fft = ytmp.shape[0]
        for frame in range(ytmp.shape[1]):
            sample = frame * hop_length
            y[sample:sample + n_fft] += ytmp[:, frame]

    def __window_sumsquare(self, window, n_frames, hop_length=512, win_length=None, n_fft=2048,
                        dtype=np.float32, norm=None):
        if win_length is None:
            win_length = n_fft

        n = n_fft + hop_length * (n_frames - 1)
        x = np.zeros(n, dtype=dtype)

        win_sq = signal.get_window(window, win_length) ** 2
        win_sq = self.__pad_center(win_sq, n_fft)

        self.__window_ss_fill(x, win_sq, n_frames, hop_length)

        return x
    
    def __window_ss_fill(self, x, win_sq, n_frames, hop_length):
        n = len(x)
        n_fft = len(win_sq)
        for i in range(n_frames):
            sample = i * hop_length
            x[sample:min(n, sample + n_fft)] += win_sq[:max(0, min(n_fft, n - sample))]
