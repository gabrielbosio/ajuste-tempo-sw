import wave
import pytest
import numpy as np
from lector import LectorAudio


@pytest.fixture
def archivo_loop():
    return wave.open("res/test_loop.wav")


def test_lector_comienza_leyendo_desde_el_principio(archivo_loop, mocker):
    loop_audio = mocker.Mock(contenido=archivo_loop.readframes(archivo_loop.getnframes()),
                             frecuencia_muestreo=archivo_loop.getframerate())

    lector = LectorAudio(loop_audio)
    numero_primera_pagina = 0
    lector.pagina_actual()

    loop_audio.pagina.assert_called_once_with(numero_primera_pagina)


def test_leer_audio_una_vez(archivo_loop, mocker):
    loop_audio = mocker.Mock(contenido=archivo_loop.readframes(archivo_loop.getnframes()),
                             frecuencia_muestreo=archivo_loop.getframerate())
    loop_audio.pagina.return_value = np.frombuffer(loop_audio.contenido[:len(loop_audio.contenido) // 8],
                                                   dtype=np.float64)

    lector = LectorAudio(loop_audio)
    cantidad_frames_a_leer = 1024
    cantidad_frames_fade = lector.cantidad_frames_fade
    longitud_lectura = (cantidad_frames_a_leer + cantidad_frames_fade * 2) * 4
    chunk_lectura = lector.leer(cantidad_frames_a_leer)
    chunk_lectura_sin_fades = chunk_lectura[cantidad_frames_fade:-cantidad_frames_fade]
    chunk_contenido = lector.loop_audio.contenido[:longitud_lectura]
    chunk_contenido_sin_fades = chunk_contenido[cantidad_frames_fade:-cantidad_frames_fade]

    assert chunk_lectura_sin_fades == chunk_contenido_sin_fades


def test_al_crear_el_lector_la_proxima_pagina_es_la_segunda(archivo_loop, mocker):
    loop_audio = mocker.Mock(contenido=archivo_loop.readframes(archivo_loop.getnframes()),
                             frecuencia_muestreo=archivo_loop.getframerate())
    loop_audio.pagina.return_value = np.frombuffer(loop_audio.contenido[:len(loop_audio.contenido) // 8],
                                                   dtype=np.float64)

    lector = LectorAudio(loop_audio)
    numero_primera_pagina = lector.numero_pagina_actual
    proxima_pagina = lector.proxima_pagina()
    longitud_pagina = len(lector.pagina_actual())
    lector.leer(longitud_pagina)

    assert lector.numero_pagina_actual == numero_primera_pagina + 1


def test_al_crear_el_lector_este_no_leyo_nada(archivo_loop, mocker):
    loop_audio = mocker.Mock(contenido=archivo_loop.readframes(archivo_loop.getnframes()),
                             frecuencia_muestreo=archivo_loop.getframerate())

    lector = LectorAudio(loop_audio)
    
    assert not lector.leyo_pagina


def test_leer_una_pagina(archivo_loop, mocker):
    loop_audio = mocker.Mock(contenido=archivo_loop.readframes(archivo_loop.getnframes()),
                             frecuencia_muestreo=archivo_loop.getframerate())
    loop_audio.pagina.return_value = np.frombuffer(loop_audio.contenido[:len(loop_audio.contenido) // 8],
                                                   dtype=np.float64)

    lector = LectorAudio(loop_audio)
    longitud_pagina = len(lector.pagina_actual())
    cantidad_frames_a_leer = 1024
    lecturas_por_pagina = longitud_pagina // cantidad_frames_a_leer
    
    for _ in range(lecturas_por_pagina):
        lector.leer(cantidad_frames_a_leer)

    assert lector.leyo_pagina


def test_al_obtener_proxima_pagina_el_lector_olvida_que_leyo(archivo_loop, mocker):
    loop_audio = mocker.Mock(contenido=archivo_loop.readframes(archivo_loop.getnframes()),
                             frecuencia_muestreo=archivo_loop.getframerate())
    loop_audio.pagina.return_value = np.frombuffer(loop_audio.contenido[:len(loop_audio.contenido) // 8],
                                                   dtype=np.float64)

    lector = LectorAudio(loop_audio)
    longitud_pagina = len(lector.pagina_actual())
    cantidad_frames_a_leer = 1024
    lecturas_por_pagina = longitud_pagina // cantidad_frames_a_leer
    
    for _ in range(lecturas_por_pagina):
        lector.leer(cantidad_frames_a_leer)
    
    lector.proxima_pagina()

    assert not lector.leyo_pagina
