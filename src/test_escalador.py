import numpy as np
import wave
import pytest
from escalador import EscaladorTiempo
from pytest import approx


def setup_module(module):
    module.ruta_loop = "res/test_loop.wav"
    module.delta = 1


@pytest.fixture
def ventana_loop(segundos_ventana):
    archivo_loop = wave.open("res/test_loop.wav")
    frecuencia_muestreo = archivo_loop.getframerate()
    muestras_ventana = frecuencia_muestreo * segundos_ventana
    audio_loop = archivo_loop.readframes(archivo_loop.getnframes())
    audio_loop = np.frombuffer(audio_loop, dtype=np.int16).astype(np.float64)
    ventana_loop = audio_loop[:muestras_ventana]

    return ventana_loop


@pytest.fixture
def segundos_ventana():
    return 3


def test_escalar_tiempo_de_loop_al_doble_reduce_longitud_a_la_mitad(ventana_loop):
    escalador = EscaladorTiempo()
    ventana_escalada = escalador.ejecutar(ventana_loop, 2)

    assert len(ventana_escalada) == approx(len(ventana_loop) // 2, rel=delta)


def test_escalar_tiempo_de_loop_al_doble_mantiene_fundamental(ventana_loop):
    escalador = EscaladorTiempo()
    ventana_escalada = escalador.ejecutar(ventana_loop, 2)

    assert fundamental(ventana_loop) == approx(fundamental(ventana_escalada), rel=delta)


def test_escalar_tiempo_de_loop_a_la_mitad_aumenta_longitud_al_doble(ventana_loop):
    escalador = EscaladorTiempo()
    ventana_escalada = escalador.ejecutar(ventana_loop, 0.5)

    assert len(ventana_escalada) == approx(len(ventana_loop) * 2, rel=delta)


def test_escalar_tiempo_de_loop_de_120_a_130_bpm(ventana_loop):
    escalador = EscaladorTiempo()
    ratio = 120 / 130
    ventana_escalada = escalador.ejecutar(ventana_loop, ratio)

    assert len(ventana_escalada) == approx(len(ventana_loop) // ratio, rel=delta)
    assert fundamental(ventana_loop) == approx(fundamental(ventana_escalada), rel=delta)


def fundamental(x):
    w = np.fft.fft(x)
    freqs = np.fft.fftfreq(len(w))
    idx = np.argmax(np.abs(w))

    return np.abs(freqs[idx])
