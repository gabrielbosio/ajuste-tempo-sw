import pytest
import wave
import numpy as np
from loop_audio import LoopAudio


@pytest.fixture
def archivo_loop():
    return wave.open("res/test_loop.wav")


def test_las_paginas_son_de_igual_longitud(archivo_loop):
    cantidad_paginas = 4
    loop_audio = LoopAudio(archivo_loop, cantidad_paginas=cantidad_paginas)
    longitud_primer_pagina = len(loop_audio.pagina(0))
    longitudes_paginas = []

    for i in range(cantidad_paginas):
        longitud_pagina = len(loop_audio.pagina(i))
        longitudes_paginas.append(longitud_pagina)

    assert longitudes_paginas == [longitud_primer_pagina] * cantidad_paginas


def test_al_llegar_al_final_se_vuelve_al_principio(archivo_loop):
    cantidad_paginas = 4
    loop_audio = LoopAudio(archivo_loop, cantidad_paginas=cantidad_paginas)
    pagina_siguiente_a_la_ultima = loop_audio.pagina(cantidad_paginas).astype(np.int16)
    primera_pagina = loop_audio.pagina(0).astype(np.int16)

    assert (pagina_siguiente_a_la_ultima == primera_pagina).all()
