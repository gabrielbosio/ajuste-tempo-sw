import numpy as np


class LoopAudio:
    def __init__(self, archivo_loop, cantidad_paginas=8):
        self.contenido = archivo_loop.readframes(archivo_loop.getnframes())
        self.frecuencia_muestreo = archivo_loop.getframerate()
        self.canales = archivo_loop.getnchannels()
        self.__dividir_contenido_en_paginas(cantidad_paginas)
    
    def pagina(self, numero_pagina):
        u"""Devuelve una página del loop de audio como array de numpy.
        numero_pagina -- Indica la página del audio a devolver, basado en la cantidad de
                         páginas en que se dividió el loop de audio.
        """
        return self.paginas[numero_pagina % len(self.paginas)]

    def __dividir_contenido_en_paginas(self, cantidad_paginas):
        vector_contenido = np.frombuffer(self.contenido, dtype=np.float64)
        relleno = np.zeros(cantidad_paginas - len(vector_contenido) % cantidad_paginas)
        vector_contenido = np.hstack((vector_contenido, relleno))
        self.paginas = np.split(vector_contenido, cantidad_paginas)
    