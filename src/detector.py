import numpy as np
from scipy import signal
import pywt


class DetectorTempo:
    
    def __init__(self, iteraciones=4, segundos_ventana=3):
        self.iteraciones = iteraciones
        self.segundos_ventana = segundos_ventana

    def ejecutar(self, ventana, fs):
        u"""Devuelve el tempo de la ventana, o un valor negativo si encuentra más de un
        tempo posible.
        ventana -- Un vector de audio al cual se le va a calcular el tempo.
        fs -- La frecuencia de muestreo de la ventana.
        """
        if len(np.shape(ventana)) > 1:
            ventana = ventana[:, 0]

        muestras_ventana = self.segundos_ventana * fs
        transformacion = self.__procesar_bandas(ventana[:muestras_ventana])

        suma_coef = np.sum(transformacion, axis=0)
        picos = np.correlate(suma_coef, suma_coef, 'full')
        periodicidad = self.__obtener_periodicidad(picos, fs)
        
        if len(periodicidad) > 1:
            return -1

        tempo = 60 / periodicidad[0]

        return tempo

    def __procesar_bandas(self, x):
        transformacion = []

        for i in range(self.iteraciones):
            coef_pasabajos, coef_pasaaltos = pywt.dwt(x if i == 0 else coef_pasabajos, 'db4')
            coef_pasaaltos = self.__procesar_coef(coef_pasaaltos, self.iteraciones - i - 1)
            transformacion.append(coef_pasaaltos)

        coef_pasabajos = self.__procesar_coef(coef_pasabajos, 0)
        transformacion.append(coef_pasabajos)
        
        return self.__normalizar_longitudes(transformacion)

    def __procesar_coef(self, coef, submuestreo):
        coef = self.__filtro_pasabajos(coef, 0.99)
        coef = coef[::2**submuestreo]
        coef = np.abs(coef)
        coef -= np.mean(coef)

        return coef
    
    def __filtro_pasabajos(self, x, alpha):
        return signal.lfilter([1 - alpha], [1, alpha], x)
    
    def __normalizar_longitudes(self, x):
        normalizado = []
        longitud_max_coef = max(map(len, x))

        for i in range(len(x)):
            paso = x[i]
            resto = np.zeros(longitud_max_coef - len(paso))
            normalizado.append(np.concatenate((paso, resto)))
        
        return normalizado

    def __obtener_periodicidad(self, picos, fs):
        submuestreo_max = 2**(self.iteraciones - 1)
        rango_pico_min = int(60 / 220 * (fs / submuestreo_max))
        rango_pico_max = int(60 / 40 * (fs / submuestreo_max))
        picos = (picos[len(picos) // 2:])[rango_pico_min:rango_pico_max]
        pico_max = np.amax(np.abs(picos))
        indice_pico_max = np.where(picos == pico_max)[0]

        if len(indice_pico_max) == 0:
            indice_pico_max = np.where(picos == -pico_max)[0]

        return (indice_pico_max + rango_pico_min) / (fs / submuestreo_max)
