import numpy as np
import pytest
from compuerta import Compuerta


def test_filtrar_array_con_todos_sus_elementos_menores_al_umbral():
    compuerta = Compuerta(umbral=1)
    x = np.array([0.5, 0.3, 0.4, 0.2])
    x_filtrada = compuerta.filtrar(x)
    x_esperada = np.array([0, 0, 0, 0])

    assert (x_filtrada == x_esperada).all()


def test_filtrar_array_con_algunos_elementos_menores_al_umbral():
    compuerta = Compuerta(umbral=1)
    x = np.array([0.5, 0.3, 1.1, 0.2, 1])
    x_filtrada = compuerta.filtrar(x)
    x_esperada = np.array([0, 0, 1.1, 0, 1])

    assert (x_filtrada == x_esperada).all()


def test_filtrar_audio_estereo_con_todas_sus_intensidades_menores_al_umbral():
    compuerta = Compuerta(umbral=1)
    x = np.array([[0.5, 0.3], [1.1, 0.2], [1, 0.5], [1.2, 1]])
    x_filtrada = compuerta.filtrar(x)
    x_esperada = np.array([[0, 0], [1.1, 0], [1, 0], [1.2, 1]])

    assert (x_filtrada == x_esperada).all()
