import multiprocessing as mp
import argparse
import procesos


def main():
    contexto = mp.get_context("spawn")
    parser = crear_parser()
    argumentos = parser.parse_args()

    print("Ctrl+C para salir")
    
    buffer_a_procesar = contexto.Queue()
    buffer_procesado = contexto.Queue()
    proceso_io = contexto.Process(target=procesos.entrada_salida,
                                  args=(buffer_a_procesar, buffer_procesado,
                                        argumentos.archivo))
    proceso_ajuste = contexto.Process(target=procesos.ajuste_tempo,
                                      args=(buffer_a_procesar, buffer_procesado))
    proceso_io.start()
    proceso_ajuste.start()
    proceso_io.join()
    proceso_ajuste.join()


def crear_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--archivo", type=str, default="res/test_loop.wav")
    
    return parser


if __name__ == "__main__":
    main()
